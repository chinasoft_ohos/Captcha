package com.luozm.captcha;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Slider;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;

/**
 * Created by luozhanming on 2018/1/17.
 */
 class TextSeekbar extends Slider implements Component.DrawTask {


    private Paint textPaint;

    public TextSeekbar(Context context) {
        super(context);
    }

    public TextSeekbar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        textPaint = new Paint();
        int textSize = Utils.dp2px(context, 14);
        textPaint.setTextSize(textSize);
        textPaint.setAntiAlias(true);
        invalidate();
    }


    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        Paint.FontMetrics fontMetrics = textPaint.getFontMetrics();
        float top = fontMetrics.top;//为基线到字体上边框的距离
        float bottom = fontMetrics.bottom;//为基线到字体下边框的距离
//        int baseLineY = (int) (getHeight() / 2 - top / 2 - bottom / 2);//基线中间点的y轴
        int baseLineY = (int) Utils.sub(Utils.sub(Utils.div(getHeight(),2),Utils.div(top,2)),Utils.div(bottom,2))  ; //基线中间点的y轴
        int div = (int) Utils.div(getWidth(), 3);
        canvas.drawText(textPaint,"向右滑动滑块完成拼图", div, baseLineY);
    }
}
