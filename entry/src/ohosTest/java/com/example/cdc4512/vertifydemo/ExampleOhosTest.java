/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.cdc4512.vertifydemo;


import com.luozm.captcha.Captcha;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleOhosTest {
    private static Captcha captcha;
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.example.cdc4512.vertifydemo", actualBundleName);
    }

    @BeforeClass
    public static void set(){
        Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        captcha = new Captcha(ability, new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        });
        captcha.setBlockSize(50);
        captcha.setMode(Captcha.MODE_BAR);
        captcha.setMaxFailedCount(3);
    }

    @Test
    public void getMode(){
        assertTrue(Captcha.MODE_BAR==captcha.getMode());
    }

    @Test
    public void getBlockSize(){
        assertTrue(50==captcha.getBlockSize());
    }

    @Test
    public void getMaxFailedCount(){
        assertTrue(3==captcha.getMaxFailedCount());
    }
}