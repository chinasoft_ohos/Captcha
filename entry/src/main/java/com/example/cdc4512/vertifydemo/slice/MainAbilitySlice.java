package com.example.cdc4512.vertifydemo.slice;

import com.example.cdc4512.vertifydemo.ResourceTable;
import com.luozm.captcha.Captcha;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.global.resource.*;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    private Captcha captcha;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        captcha = (Captcha) findComponentById(ResourceTable.Id_captcha);
        Button btnMode = (Button) findComponentById(ResourceTable.Id_btn_mode);
        findComponentById(ResourceTable.Id_btn_mode).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (captcha.getMode() == Captcha.MODE_BAR) {
                    captcha.setMode(Captcha.MODE_NONBAR);
                    btnMode.setText("滑动条模式");
                } else {
                    captcha.setMode(Captcha.MODE_BAR);
                    btnMode.setText("无滑动条模式");
                }
            }
        });

        captcha.setCaptchaListener(new Captcha.CaptchaListener() {
            @Override
            public String onAccess(long time) {
                new ToastDialog(getContext())
                        .setText( "验证成功")
                        .show();
                return "验证通过";
            }

            @Override
            public String onFailed(int count) {
                new ToastDialog(getContext())
                        .setText( "验证失败,失败次数"+ count)
                        .show();
                return "验证失败";
            }

            @Override
            public String onMaxFailed() {
                new ToastDialog(getContext())
                        .setText( "验证超过次数，你的帐号被封锁")
                        .show();
                return "可以走了";
            }

        });
        findComponentById(ResourceTable.Id_changeimg).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
//                new ToastDialog(getContext())
//                        .setText("更换图片")
//                        .show();
//                changePicture();
            }
        });
        findComponentById(ResourceTable.Id_btn_slider).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(getContext())
                        .setText("更换样式")
                        .show();
                changeProgressDrawable();
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    boolean isCat = true;

    public void changePicture() {
        if (isCat) {
            captcha.setBitmap("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fa0.att.hudong.com%2F30%2F29%2F01300000201438121627296084016.jpg&refer=http%3A%2F%2Fa0.att.hudong.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1616745001&t=397f96c00633c34d0d61f7031a934790");
        } else {
            try {
                ResourceManager manager =getResourceManager();
                String  mediaPath = getResourceManager().getMediaPath(ResourceTable.Media_cat1);
                Resource resource1 = manager.getRawFileEntry(mediaPath).openRawFile();
                ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
                ImageSource imageSource = ImageSource.create(resource1,sourceOptions);
                ImageSource.DecodingOptions   decodingOptions = new ImageSource.DecodingOptions();
                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                captcha.setBitmap(pixelMap);
                pixelMap.release();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }
        isCat = !isCat;
    }

    boolean isSeekbar1 = false;
    public void changeProgressDrawable(){
        if(isSeekbar1){
            captcha.setSeekBarStyle(ResourceTable.Graphic_po_seekbar,ResourceTable.Graphic_thumb);
        }else{
            captcha.setSeekBarStyle(ResourceTable.Graphic_po_seekbar1,ResourceTable.Graphic_thumb1);
        }
        isSeekbar1=!isSeekbar1;
    }
}
