# Captcha


#### 项目介绍
- 项目名称：滑块拼图验证码控件
- 所属系列：openharmony的第三方组件适配移植
- 功能：滑块拼图验证码控件
- 项目移植状态：主功能完成，滑块效果有差异
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本： Release  1.12

#### 效果演示
<img src="image/1.gif"></img>

#### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:captcha:1.0.2')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

组件主要由Img控件和Slider控件组成。

 1.将Captcha添加至布局
```xml
<com.luozm.captcha.Captcha
        ohos:id="$+id:captcha"
        ohos:width="match_parent"
        ohos:height="match_content"
        app:src="$media:cat"/>
```
 2.添加Java代码

```Java
   captcha = (Captcha) findComponentById(ResourceTable.Id_captCha);
   captcha.setCaptchaListener(new Captcha.CaptchaListener() {
            @Override
            public String onAccess(long time) {
                new ToastDialog(getContext())
                        .setText( "验证成功")
                        .show();
                return "验证通过";
            }

            @Override
            public String onFailed(int failedCount) {
               new ToastDialog(getContext())
                        .setText( "验证失败,失败次数"+ count)
                        .show();
                return "验证失败";
            }

            @Override
            public String onMaxFailed() {
               new ToastDialog(getContext())
                        .setText( "验证超过次数，你的帐号被封锁")
                        .show();
                return "验证超过次数，你的帐号被封锁";
            }
        });
```
 4.(可选)自定义拼图样式<br>
 1）编写策略类,继承CaptchaStrategy类，重写策略方法,具体可参考DefaultCaptchaStrategy类
  ```Java
  public abstract class CaptchaStrategy {

    protected Context mContext;

    public CaptchaStrategy(Context ctx) {
        this.mContext = ctx;
    }

    protected Context getContext() {
        return mContext;
    }

    /**
     * 定义拼图缺块的形状
     *
     * @param blockSize 单位dp，注意转化为px,缺块的大小，注意Path的边界不要超出此大小
     * @return path of the shape
     */
    public abstract Path getBlockShape(int blockSize);

    /**
     * 根据整张拼图的宽高和拼图缺块大小定义拼图缺块的位置
     *
     * @param width  picture width
     * @param height picture height
     * @return position info of the block
     */
    public abstract PositionInfo getBlockPostionInfo(int width, int height, int blockSize);

    /**
     * 定义滑块图片的位置信息(只有设置为无滑动条模式有用并建议重写)
     *
     * @param width  picture width
     * @param height picture height
     * @return position info of the block
     */
    public PositionInfo getPositionInfoForSwipeBlock(int width, int height, int blockSize){
        return getBlockPostionInfo(width,height,blockSize);
    }

    /**
     * 定义拼图缺失部分阴影的Paint
     */
    public abstract Paint getBlockShadowPaint();

    /**
     * 获得拼图缺块图片的Paint
     */
    public abstract Paint getBlockBitmapPaint();

    /**
     * 装饰滑块图片，在绘制图片后执行，即绘制滑块前景
     * @params canvas
     * @params shape   缺块的形状
     */
    public void decoreateSwipeBlockBitmap(Canvas canvas, Path shape) {

    }
}

  ```
 2）添加Java代码
```Java
captCha.setCaptchaStrategy(new XXXCaptchaStrategy(context));
```

 5.(可选)自定义滑块条
   与Slider自定义样式一样
```xml
<com.luozm.captcha.Captcha
    ohos:id="$+id:captcha"
    ohos:width="match_parent"
    ohos:height="match_content"
    app:mode="mode_bar"
    app:src="$media:cat1"
    app:blockSize="150"
    app:max_fail_count="5"
    app:progressbg="$graphic:po_seekbar"
    app:thumbbg="$graphic:thumb"/>
```

```Java
captcha.setSeekBarStyle(ResourceTable.Graphic_po_seekbar,ResourceTable.Graphic_thumb);
```

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.2

